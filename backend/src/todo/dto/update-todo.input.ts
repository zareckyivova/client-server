import { InputType, PartialType, OmitType } from '@nestjs/graphql';
import { Todo } from '../todo.entity';

@InputType()
export class UpdateTodoInput extends OmitType(PartialType(Todo, InputType), [
  'id',
]) {}
