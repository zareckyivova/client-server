import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
} from '@nestjs/common';
import { TodoService } from './todo.service';
import { Todo } from './todo.entity';
import { UpdateTodoInput } from './dto/update-todo.input';

@Controller('todos')
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  @Get()
  async findAll(): Promise<Todo[]> {
    return this.todoService.findAll();
  }

  @Post()
  async create(
    @Body() { title, description }: { title: string; description: string },
  ): Promise<Todo> {
    return this.todoService.create(title, description);
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateBody: UpdateTodoInput,
  ): Promise<void> {
    await this.todoService.update(id, updateBody);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<boolean> {
    return this.todoService.delete(id);
  }
}
