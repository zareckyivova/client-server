import { Resolver, Query, Args, Mutation, ID } from '@nestjs/graphql';
import { TodoService } from './todo.service';
import { Todo } from './todo.entity';
import { CreateTodoInput } from './dto/create-todo.input';
import { UpdateTodoInput } from './dto/update-todo.input';

@Resolver(() => Todo)
export class TodoResolver {
  constructor(private todoService: TodoService) {}

  @Query(() => [Todo])
  async todos(): Promise<Todo[]> {
    return this.todoService.findAll();
  }

  @Query(() => Todo, { nullable: true })
  async todoById(@Args('id', { type: () => ID }) id: string): Promise<Todo> {
    return this.todoService.findById(id);
  }

  @Mutation(() => Todo)
  async createTodo(@Args('input') input: CreateTodoInput): Promise<Todo> {
    return this.todoService.create(input.title, input.description);
  }

  @Mutation(() => Boolean, { nullable: true })
  async updateTodo(
    @Args('id', { type: () => ID }) id: string,
    @Args('input') input: UpdateTodoInput,
  ): Promise<boolean> {
    const result = await this.todoService.update(id, input);

    return !!result.affected;
  }

  @Mutation(() => Boolean)
  async deleteTodo(
    @Args('id', { type: () => ID }) id: string,
  ): Promise<boolean> {
    return this.todoService.delete(id);
  }
}
