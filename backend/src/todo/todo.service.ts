import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { Todo } from './todo.entity';
import { UpdateTodoInput } from './dto/update-todo.input';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(Todo)
    private todoRepository: Repository<Todo>,
  ) {}

  async findAll(): Promise<Todo[]> {
    return this.todoRepository.find();
  }
  async findById(id: string): Promise<Todo> {
    return this.todoRepository.findOneBy({ id });
  }

  async create(title: string, description: string): Promise<Todo> {
    const todo = new Todo();
    todo.title = title;
    todo.description = description;
    todo.completed = false;
    return this.todoRepository.save(todo);
  }

  async update(id: string, input: UpdateTodoInput): Promise<UpdateResult> {
    return this.todoRepository.update(id, input);
  }

  async delete(id: string): Promise<boolean> {
    const result = await this.todoRepository.delete(id);

    return !!result.affected;
  }
}
