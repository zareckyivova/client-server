import React, { useState, useEffect } from "react";
import { 
  AppBar, 
  Toolbar, 
  Typography, 
  Container, 
  Grid, 
  Paper, 
  TextField, 
  Button, 
  Table, 
  TableBody, 
  TableCell, 
  TableContainer, 
  TableHead, 
  TableRow,
  Box
} from "@mui/material";
import { useQuery, useMutation } from "@apollo/client";
import { GET_ALL, ADD_TODO, UPDATE_TODO, DELETE_TODO } from "./api/graphql";

import Todo from "./components/Todo";

import "./App.css";

function App() {
  const [todos, setTodos] = useState([]);
  const [newTodoTitle, setNewTodoTitle] = useState("");
  const [newTodoDescription, setNewTodoDescription] = useState("");
  const { loading, data } = useQuery(GET_ALL);
  const [addTodo] = useMutation(ADD_TODO);
  const [updateTodo] = useMutation(UPDATE_TODO);
  const [deleteTodo] = useMutation(DELETE_TODO);

  useEffect(() => {
    if (data?.todos) {
      setTodos(data.todos);
    }
  }, [data, data?.todos]);

  const handleAddTodo = (e) => {
    e.preventDefault();
    addTodo({
      variables: { title: newTodoTitle, description: newTodoDescription },
    })
      .then((res) => {
        setTodos([...todos, res.data.createTodo]);
      })
      .catch((error) => console.error(error));
  };

  const handleDeleteTodo = (id) => {
    deleteTodo({ variables: { id } })
      .then(() => setTodos(todos.filter((todo) => todo.id !== id)))
      .catch((error) => console.error(error));
  };

  const handleUpdate = (input) => {
    return updateTodo({ variables: input })
      .then((res) => {
        const { id, ...inputFields } = input;
        if (res?.data?.updateTodo) {
          setTodos((state) => {
            return state.map((todo) => {
              if (todo.id === input.id) {
                return { ...todo, ...inputFields };
              }
              return todo;
            });
          });

          return res?.data?.updateTodo;
        }
      })
      .catch((error) => console.error(error));
  };

  const handleCompleted = (id, completed) => {
    updateTodo({ variables: { id, completed } })
      .then((res) => {
        if (res?.data?.updateTodo) {
          setTodos((state) => {
            return state.map((todo) => {
              if (todo.id === id) {
                return { ...todo, completed };
              }
              return todo;
            });
          });
        }
      })
      .catch((error) => console.error(error));
  };

  return (
    <>
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6">Todo List</Typography>
      </Toolbar>
    </AppBar>
    <Container maxWidth="md">
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper sx={{ mt: "8px" }}>
            <Box component="form" onSubmit={handleAddTodo}>
              <TextField
                label="Add new todo"
                variant="outlined"
                size="small"
                fullWidth
                value={newTodoTitle}
                onChange={(e) => setNewTodoTitle(e.target.value)}
              />
              <TextField
                label="Todo description (Optional)."
                variant="outlined"
                size="small"
                sx={{ mt: "4px" }}
                fullWidth
                value={newTodoDescription}
                onChange={(e) => setNewTodoDescription(e.target.value)}
              />
              <Button
                type="submit"
                variant="contained"
                sx={{ mt: "4px" }}
                size="large"
                disabled={!newTodoDescription}
              >
                Add
              </Button>
            </Box>
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper>
            {loading ? (
              <p>Loading...</p>
            ) : (
              <TableContainer>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>Title</TableCell>
                      <TableCell align="right">Actions</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {todos.map((todo) => (
                      <Todo
                        todo={todo}
                        handleDeleteTodo={handleDeleteTodo}
                        handleUpdate={handleUpdate}
                        handleCompleted={handleCompleted}
                        key={todo.id}
                      />
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            )}
          </Paper>
        </Grid>
      </Grid>
    </Container>
  </>
  );
}

export default App;
