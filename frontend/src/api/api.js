const BASE_URL = "http://localhost:3000";

const getItems = async () => {
  const response = await fetch(`${BASE_URL}/todos`, {headers: {
    "Access-Control-Allow-Origin": "http://localhost:8080"
  }});
  const data = await response.json();
  return data;
};

const getItem = async (id) => {
  const response = await fetch(`${BASE_URL}/todos/${id}`);
  const data = await response.json();
  return data;
};

const createItem = async (item) => {
  const response = await fetch(`${BASE_URL}/todos`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "http://localhost:8080"
    },
    body: JSON.stringify(item),
  });
  const data = await response.json();
  return data;
};

const updateItem = async (id, updates) => {
  const response = await fetch(`${BASE_URL}/todos/${id}`, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(updates),
  });
  const data = await response.json();
  return data;
};

const deleteItem = async (id) => {
  const response = await fetch(`${BASE_URL}/todos/${id}`, {
    method: "DELETE",
  });
  const data = await response.json();
  return data;
};

export { getItems, getItem, createItem, updateItem, deleteItem };