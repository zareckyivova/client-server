import { gql } from "@apollo/client";

export const GET_ALL = gql`
  query getAllTodos {
    todos {
      id
      title
      description
      completed
    }
  }
`;

export const UPDATE_TODO = gql`
  mutation UpdateTodo(
    $id: ID!
    $title: String
    $description: String
    $completed: Boolean
  ) {
    updateTodo(
      id: $id
      input: { title: $title, description: $description, completed: $completed }
    )
  }
`;

export const ADD_TODO = gql`
  mutation AddTodo($title: String!, $description: String) {
    createTodo(input: { title: $title, description: $description }) {
      id
      title
      description
      completed
    }
  }
`;

export const DELETE_TODO = gql`
  mutation DeleteTodo($id: ID!) {
    deleteTodo(id: $id)
  }
`;