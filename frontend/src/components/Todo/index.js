import React, { useState } from "react";
import {
  TextField,
  Button,
  TableCell,
  TableRow,
  Checkbox,
} from "@mui/material";

import "./Todo.css";

function Todo({ todo, handleDeleteTodo, handleUpdate, handleCompleted }) {
  const [updateState, setUpdateState] = useState(false);
  const [updateTitle, setUpdateTitle] = useState(todo.title || "");
  const [updateDescription, setUpdateDescription] = useState(
    todo.description || ""
  );
  const toggleUpdate = () => {
    setUpdateState(!updateState);
    setUpdateDescription(todo.description || "");
    setUpdateTitle(todo.title || "");
  };

  const handleUpdateConfirm = async () => {
    const result = await handleUpdate({
      id: todo.id,
      title: updateTitle,
      description: updateDescription,
    });
    if (result) {
      toggleUpdate();
    }
  };

  return (
    <TableRow>
      <TableCell component="th" scope="row">
        {updateState ? (
          <TextField
            variant="outlined"
            size="small"
            fullWidth
            value={updateTitle}
            onChange={(e) => setUpdateTitle(e.target.value)}
          />
        ) : (
          <p>{todo.title}</p>
        )}
        {updateState ? (
          <TextField
            variant="outlined"
            size="small"
            fullWidth
            value={updateDescription || ""}
            onChange={(e) => setUpdateDescription(e.target.value)}
          />
        ) : todo.description ? (
          <p className="description">{todo.description}</p>
        ) : null}
      </TableCell>

      <TableCell align="right">
        {!updateState && (
          <Checkbox
            checked={todo.completed}
            onChange={(e) => {
              handleCompleted(todo.id, e.target.checked);
            }}
          />
        )}
        {updateState && (
          <Button
            variant="contained"
            color="primary"
            onClick={handleUpdateConfirm}
            sx={{ ml: "4px" }}
          >
            Confirm
          </Button>
        )}
        <Button
          variant="contained"
          color={updateState ? "error" : "primary"}
          onClick={toggleUpdate}
          sx={{ ml: "4px" }}
        >
          {updateState ? "Cancel" : "Update"}
        </Button>
        {!updateState && (
          <Button
            variant="contained"
            color="error"
            onClick={() => handleDeleteTodo(todo.id)}
            sx={{ ml: "4px" }}
          >
            Delete
          </Button>
        )}
      </TableCell>
    </TableRow>
  );
}

export default Todo;
