import React from 'react';
import ReactDOM from 'react-dom/client';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import './index.css';
import App from './App';

console.log("process.env",process.env );
console.log("REACT_APP_BACKEND_URL",process.env.REACT_APP_BACKEND_URL );
const client = new ApolloClient({
  uri: `${process.env.REACT_APP_BACKEND_URL || 'http://backend:3000'}/graphql`,
  connectToDevTools: true,
  cache: new InMemoryCache(),
});

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </React.StrictMode>
);
